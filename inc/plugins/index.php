<?php
/**
 * Here include all plugins related functions
 *
 */
// install default plugins
include( get_template_directory() . '/inc/plugins/defaults.php' );
// general plugin functions
include( get_template_directory() . '/inc/plugins/general.php' );
// jetpack plugin custom functions
include( get_template_directory() . '/inc/plugins/jetpack.php' );