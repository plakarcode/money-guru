<?php
/**
 * The footer
 *
 * Contains footer content and the closing of the
 * body and html.
 *
 * @package WordPress
 */
?>

	<footer id="mainfooter" class="footer-main" role="contentinfo">
		<div class="footer-main__top">
			<div class="justifize">
				<div class="justifize__box">
					<div class="mb--"><?php echo house_svg_icon( 'logotype' ); ?></div>
					<p class="copyright"><?php echo house_footer_copyrights(); ?></p>
				</div><!-- justifize__box -->

				<div class="justifize__box">
					<a class="smooth-scroll" href="#top"><?php echo house_svg_icon( 'arrow-up' ); ?></a>
				</div><!-- justifize__box -->

				<div class="justifize__box">
					<ul class="footer-main__social">
						<?php // get_template_part( 'partials/content/social' ); ?>
					</ul><!-- footer-main__social -->
				</div><!-- justifize__box -->
			</div><!-- justifize -->
		</div><!-- footer-main__top -->

		<div class="footer-main__bottom">
			<div class="justifize">
				<div class="justifize__box footer-main__bottom__links">
					<?php
						/**
						 * Check if we want terms and privacy
						 * @var bool
						 */
						$terms = get_field( 'show_terms_and_conditions' );
						$privacy = get_field( 'show_privacy_policy' );

						if ( $terms ) :	?>
							<a class="btn--openpopup" href="#terms-lightbox">Terms &amp; Conditions</a>
						<?php endif; // $terms

						if ( $terms && $privacy ) :	?>
							<span class="mh--">|</span>
						<?php endif; // $terms && $privacy

						if ( $privacy ) : ?>
							<a class="btn--openpopup" href="#privacy-lightbox">Privacy Policy</a>
						<?php endif; // $privacy
					?>

				</div><!-- justifize__box footer-main__bottom__links -->

				<div class="justifize__box">
					<a class="top-link smooth-scroll" href="#top">Back To Top</a>
				</div><!-- justifize__box -->

				<div class="justifize__box">
					<a href="http://www.egzote.com" target="_blank">Made at The House</a>
				</div><!-- justifize__box -->
			</div><!-- justifize -->
		</div><!-- footer-main__bottom -->
	</footer><!-- #mainfooter -->

	<footer class="footer-main--mobile">
		<div class="footer-main__top">
			<a class="smooth-scroll" href="#top"><?php echo house_svg_icon( 'arrow-up' ); ?></a>

			<ul class="footer-main__social mt+">
				<?php get_template_part( 'partials/content/social' ); ?>
			</ul><!-- footer-main__social mt+ -->

			<div class="footer-main__bottom__links mv">

				<?php
					/**
					 * Check if we want terms and privacy
					 * @var bool
					 */
					$terms = get_field( 'show_terms_and_conditions' );
					$privacy = get_field( 'show_privacy_policy' );

					if ( $terms ) :	?>
						<a class="btn--openpopup" href="#terms-lightbox">Terms &amp; Conditions</a>
					<?php endif; // $terms

					if ( $terms && $privacy ) :	?>
						<span class="mh--">|</span>
					<?php endif; // $terms && $privacy

					if ( $privacy ) : ?>
						<a class="btn--openpopup" href="#privacy-lightbox">Privacy Policy</a>
					<?php endif; // $privacy
				?>

			</div><!-- footer-main__bottom__links mv -->

			<div class="mb--">
				<?php echo house_svg_icon( 'logotype' ); ?>
			</div><!-- mb-- -->

			<p class="copyright"><?php echo house_footer_copyrights(); ?></p>
		</div><!-- footer-main__top -->

		<div class="footer-main__bottom">
			<a href="http://www.egzote.com" target="_blank">Made at The House</a>
		</div><!-- footer-main__bottom -->
	</footer><!-- footer-main--mobile -->

	<?php get_template_part( 'partials/content/popup-terms' ); ?>
	<?php get_template_part( 'partials/content/popup-privacy' ); ?>
	<?php get_template_part( 'partials/meta/google-analytics' ); ?>

<?php wp_footer(); ?>
</body>
</html>