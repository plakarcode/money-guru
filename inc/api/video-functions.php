<?php
/**
 * YouTube and Vimeo API functions
 *
 * @package WordPress
 */
/**
 * Get video host
 *
 * Check where video is coming from. We are particularly interested in
 * youtube or vimeo.
 *
 * @param  string $url Video url
 * @return string      Returns 'host' part of video url - domain
 */
function get_video_host( $url ) {
	$parse = parse_url( $url );
	$host = $parse['host'];

	return $host;
}
/**
 * Set video image
 *
 * Check where is video coming from and set its image.
 *
 * @param string $url         Video url
 * @return string             Returns video image src
 */
function set_video_image_src( $url ) {
	/**
	 * Check the host
	 * @var string
	 */
	$host = get_video_host( $url );
	/**
	 * Declare src
	 * @var string
	 */
	$src = '';

	if ( strpos( $host, 'youtu' ) !== false ) {
		/**
		 * Get youtube image src
		 * @var string
		 */
		$src = get_youtube_video_full_image( $url );

	} elseif ( strpos( $host, 'vimeo' ) !== false ) {
		/**
		 * Get vimeo image src
		 * @var string
		 */
		$src = get_vimeo_video_image( $url, $size = 'large' );
	}

	return $src;
}
/**
 * Set iframe src
 *
 * The only difference between youtube and vimeo <iframe> is in src attribute.
 * Let's build it now depending on video host.
 *
 * @param string $url         Video url
 * @param string $iframe_id   HTML id for <iframe>
 * @param string $autoplay    Autoplay control for player, '0' and '1' are valid values
 * @param string $loop        Loop control for player, '0' and '1' are valid values
 * @return string             Returns <iframe> ready url
 */
function set_iframe_src( $url, $iframe_id = 'video-player', $loop = '0' ) {
	/**
	 * Check the host
	 * @var string
	 */
	$host = get_video_host( $url );
	/**
	 * Declare src
	 * @var string
	 */
	$src = '';

	if ( strpos( $host, 'youtu' ) !== false ) {
		/**
		 * Get video id
		 * @var string
		 */
		$video_id = get_youtube_video_id( $url );
		/**
		 * Build youtube iframe src
		 * @var string
		 */
		$src = 'https://www.youtube.com/embed/' . $video_id . '?loop=' . $loop . '&enablejsapi=1';

	} elseif ( strpos( $host, 'vimeo' ) !== false ) {
		/**
		 * Get video id
		 * @var string
		 */
		$video_id = get_vimeo_id_by_url( $url );
		/**
		 * Build vimeo iframe src
		 * @var string
		 */
		$src = 'https://player.vimeo.com/video/' . $video_id . '?loop=' . $loop . '&api=1&player_id=' . $iframe_id;
	}

	return $src;
}
/**
 * Build video iframe
 *
 * Build video iframe.
 *
 * @param string $url         Video url
 * @param string $iframe_id   HTML id for <iframe>
 * @param string $width       Width for the iframe
 * @param string $height      Height for the iframe
 * @param string $autoplay    Autoplay control for player, '0' and '1' are valid values
 * @param string $loop        Loop control for player, '0' and '1' are valid values
 * @return string             Returns <iframe> markup
 */
function build_video_iframe( $url, $iframe_id = 'video-player', $width = '100%', $height = '100%', $loop = '0' ) {
	$src = set_iframe_src( $url, $iframe_id, $loop );

	if ( $src ) {
		$iframe = '<iframe class="embedded__item"
						id="' . $iframe_id . '"
						src="' . $src . '"
						width="' . $width . '"
						height="' . $height . '"
						frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen>
					</iframe>';
	} else {
		$iframe = '';
	}

	return $iframe;
}