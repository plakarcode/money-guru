<?php
/**
 * Subscribe Thank you message
 *
 * Template part for rendering subscribe thank you message
 *
 * @package WordPress
 */
if ( get_field( 'subscribe_thank_you_message' ) ) : ?>

	<div class="container">
		<?php echo house_svg_icon( 'logomark' ); ?>
		<h1><?php the_field( 'subscribe_thank_you_message' ); ?></h1>
	</div><!-- container -->

<?php endif; // get_field( 'subscribe_thank_you_message' )