<?php
/**
 * Terms Popup
 *
 * Template part for rendering terms and conditions popup
 *
 * @package WordPress
 */
if ( ! get_field( 'show_terms_and_conditions' ) ) {
	return;
}
?>

<div class="terms-lightbox magnific-popup mfp-hide magnific-animate" id="terms-lightbox">
	<div class="terms-popup__content ph+ pv+ large-and-up-ph+">
		<div class="mfp-close"><?php echo house_svg_icon( 'close' ); ?></div>

		<?php if ( get_field( 'terms_popup_title' ) ) : ?>
			<h2><?php the_field( 'terms_popup_title' ); ?></h2>
		<?php endif; // get_field( 'terms_popup_title' )?>

		<?php
			/**
			 * Start flexible fields
			 */
			while ( the_flexible_field( 'terms_popup_content' ) ) :

				// START HEADING
				if ( get_row_layout() == 'popup_heading' ) : ?>
					<h3><?php the_sub_field( 'heading' ); ?></h3>
				<?php // END HEADING

				// START HIGHLIGHTED
				elseif ( get_row_layout() == 'popup_highlighted' ) : ?>
					<p class="featured"><?php the_sub_field( 'highlighted_paragraph' ); ?></p>
				<?php // END HIGHLIGHTED

				// START paragraph
				elseif ( get_row_layout() == 'popup_paragraph' ) : ?>
					<p><?php the_sub_field( 'paragraph' ); ?></p>
				<?php // END paragraph

				endif; // get_row_layout()

			endwhile; // the_flexible_field( 'terms_popup_content' )
		?>
	</div><!-- terms-popup__content ph+ pv+ large-and-up-ph+ -->
</div><!-- terms-lightbox magnific-popup mfp-hide magnific-animate -->