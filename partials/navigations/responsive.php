<?php
/**
 * Responsive navigation template part
 *
 * Template part for rendering responsive navigation.
 *
 * @link https://codex.wordpress.org/Function_Reference/wp_nav_menu
 *
 * @package WordPress
 */
?>
<nav id="responsive-navigation" class="res-menu" role="navigation">
	<?php wp_nav_menu( array( 'theme_location' => 'responsive', 'menu_class' => 'layout', 'container' => 'ul' ) ); ?>
	<ul class="layout mt">
		<li class="layout__item">
			<a href="#share-lightbox" class="btn--openpopup res-menu__item">Share</a>
		</li>
	</ul>	
</nav><!-- #responsive-navigation -->
