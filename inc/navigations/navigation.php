<?php
/**
 * Navigation
 *
 * Custom navigations functionality.
 *
 * @package WordPress
 */
/**
 * Add last nav item
 *
 * Add custom markup, too complex for dashboard menu modifications, as last
 * item in menu.
 *
 * @param string $items  HTML menu items list
 * @param obj $args      Object containing wp_nav_menu() arguments
 *
 * @link https://developer.wordpress.org/reference/hooks/wp_nav_menu_items/
 * @return string        Returns menu markup with appended custom html
 */
function add_last_nav_item( $items, $args ) {
	global $globalSite;
	if ( $args->theme_location == 'primary' ) {
		$items .= '<li class="js-tooltip-wrap tooltip-wrap">
						<a href="javascript:;">
							<span>Share</span>
							<span class="ml">' . house_svg_icon( 'share' ) . '</span>
						</a>
						<ul class="tooltip">
							<li>' . get_share_link( 'facebook', $globalSite['home'] ) . '</li>
							<li>' . get_share_link( 'twitter', $globalSite['home'], 'Check out MoneyGuru - an online investment platform combined, financial news and portfolio monitoring.', 'MoneyGuru' ) . '</li>
						</ul>
					</li>';

		return $items;
	}
}
