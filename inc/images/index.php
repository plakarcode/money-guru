<?php
/**
 * Here include all images related functions
 */

// Get the Image - An advanced post image script for WordPress by Justin Tadlock
include( get_template_directory() . '/inc/images/get-the-image.php' );
// general images functions
include( get_template_directory() . '/inc/images/general.php' );
// get default and theme defined image sizes for featured images
include( get_template_directory() . '/inc/images/featured-images.php' );
// getting the theme graphics - from 'images' and 'icons' folders
include( get_template_directory() . '/inc/images/theme-graphics.php' );