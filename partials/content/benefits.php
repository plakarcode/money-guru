<?php
/**
 * Benefits
 *
 * Template part for rendering benefits section
 *
 * @package WordPress
 */
$benefits = get_field( 'benefits' );

if ( $benefits ) : ?>

<section id="benefits" class="benefits" data-stellar-background-ratio="0.2">
	<div class="container--guru pv+ medium-and-up-pv++">
		<div class="layout">

		<?php foreach ( $benefits as $key => $benefit ) :

			/**
			 * Set 'offset' class to every second item - odd
			 * @var array
			 */
			if ( $key & 1 ) {
				// odd
				$offset = 'offset';
			} else {
				// even
				$offset = '';
			}

			/**
			 * Set margin to 0 for last item
			 * @var array
			 */
			if ( $benefit == end( $benefits ) ) {
				$margin = 'mb0';
			} else {
				$margin = '';
			}

			$lightbox_id = sanitize_title_with_dashes( $benefit['benefit_title'] );
			$card_color = $benefit['benefit_card_color'];
			$card_icon = $benefit['benefit_card_icon']; ?>

			<div class="layout__item large-and-up-1/2 benefits__card-wrap <?php echo $offset; ?> <?php echo $margin; ?>">
				<a href="#benefit-<?php echo $lightbox_id; ?>" class="btn--openpopup">
					<div class="benefits__card benefits__card--<?php echo $card_color; ?>">
						<?php echo house_svg_icon( $card_icon, 'icon-large-circle' ); ?>
					</div><!-- benefits__card benefits__card--<?php echo $card_color; ?> -->
				</a><!-- btn--openpopup -->

				<h3><a href="#benefit-<?php echo $lightbox_id; ?>" class="btn--openpopup"><?php echo $benefit['benefit_title']; ?></a></h3>
				<hr class="benefits__seperator">
				<p><?php echo $benefit['benefit_description']; ?>
			</div><!-- layout__item large-and-up-1/2 benefits__card-wrap -->

		<?php endforeach; // $benefits as $benefit ?>

		</div><!-- layout -->
	</div><!-- container--guru pv+ medium-and-up-pv++ -->
</section><!-- #benefits -->

<?php foreach ( $benefits as $benefit ) :
	$lightbox_id = sanitize_title_with_dashes( $benefit['benefit_title'] );
	/**
	 * Only one popup title differs from benefit title
	 */
	if ( $benefit['benefit_popup_title'] ) {
		$title = $benefit['benefit_popup_title'];
	} else {
		$title = $benefit['benefit_title'];
	} ?>

	<div id="benefit-<?php echo $lightbox_id; ?>" class="benefit-lightbox magnific-popup mfp-hide magnific-animate">
		<div class="benefit-popup__content pv+ ph large-and-up-p++">
			<div class="mfp-close"><?php echo house_svg_icon( 'close' ); ?></div>
			<h3><?php echo ucwords( $title ); ?></h3>
			<?php echo $benefit['benefit_popup_content']; ?>

		</div><!-- benefit-popup__content pv+ ph large-and-up-p++ -->
	</div><!-- benefit-lightbox magnific-popup mfp-hide magnific-animate -->

<?php endforeach; // $benefits as $benefit ?>

<?php endif; // $benefits