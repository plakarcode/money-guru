<?php
/**
 * Subscribe
 *
 * Template part for rendering subscribe section
 *
 * @package WordPress
 */

/**
 * Get MailChimp ids
 * @var string
 */
$list = get_field( 'mailchimp_list_id' );
$campaign = get_field( 'mailchimp_campaign_id' );

/**
 * Set button label
 */
if ( get_field( 'subscribe_button_label' ) ) {
	$subscribe = get_field( 'subscribe_button_label' );
} else {
	$subscribe = 'Subscribe';
}
?>

<div id="subscribe">
	<section class="subscribe subscribe--extralg" id="subscribe-extralg">
		<div class="container--guru subscribe__content">
			<div class="layout">
				<div class="layout__item large-and-up-1/2 anim anim--left">
					<?php if ( get_field( 'subscribe_section_title' ) ) : ?>
						<h1><?php the_field( 'subscribe_section_title' ); ?></h1>
					<?php endif; // get_field( 'subscribe_section_title' ) ?>

					<?php if ( get_field( 'subscribe_section_text' ) ) : ?>
						<p><?php the_field( 'subscribe_section_text' ); ?></p>
					<?php endif; // get_field( 'subscribe_section_text' ) ?>

					<!-- Begin MailChimp Signup Form -->
					<div id="mc_embed_signup">
						<form action="//moneyguruinvest.us13.list-manage.com/subscribe/post?u=<?php echo $campaign; ?>&amp;id=<?php echo $list; ?>" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
							<div id="mc_embed_signup_scroll" class="layout">
								<div class="layout__item large-and-up-3/4">
									<input type="email" value="" name="EMAIL" class="email input input--simple" id="mce-EMAIL" placeholder="Your Email" required>
								</div><!-- layout__item large-and-up-3/4 -->

								<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
								<div style="position: absolute; left: -5000px;">
									<input type="text" name="b_<?php echo $campaign; ?>_<?php echo $list; ?>" tabindex="-1" value="">
								</div>

								<div class="layout__item large-and-up-1/4">
									<button onchange="return this.form.submit()" name="subscribe" id="mc-embedded-subscribe" class="btn btn--primary btn--subscribe"><?php echo $subscribe; ?></button>
								</div><!-- layout__item large-and-up-1/4 -->
							</div><!-- layout -->
						</form>
					</div><!-- #mc_embed_signup -->
					<!--End mc_embed_signup-->

				</div><!-- layout__item large-and-up-1/2 anim anim--left -->

				<div class="layout__item large-and-up-1/2 hide-md anim anim--right">
					<?php
						/**
						 * Get image
						 * @var array
						 */
						$image = get_field( 'subscribe_section_image' );

						if ( $image ) :
							$src = $image['url']; ?>
							<img class="subscribe__mac" src="<?php echo $src; ?>">
						<?php endif; // $image ?>
				</div><!-- layout__item large-and-up-1/2 hide-md anim anim--right -->
			</div><!-- layout -->
		</div><!-- container--guru subscribe__content -->
		<div class="subscribe__thank-you">
			<?php echo get_template_part( 'partials/content/subscribe', 'thank-you' ); ?>
		</div><!-- subscribe__thank-you -->
	</section><!-- subscribe subscribe--extralg -->

	<section class="subscribe subscribe--res" id="subscribe-res">
		<div class="container--guru subscribe__content">
			<?php if ( get_field( 'subscribe_section_title' ) ) : ?>
				<h1 class="mb"><?php the_field( 'subscribe_section_title' ); ?></h1>
			<?php endif; // get_field( 'subscribe_section_title' ) ?>

			<?php if ( get_field( 'subscribe_section_text' ) ) : ?>
				<p><?php the_field( 'subscribe_section_text' ); ?></p>
			<?php endif; // get_field( 'subscribe_section_text' ) ?>

			<!-- Begin MailChimp Signup Form -->
			<div id="mc_embed_signup">
				<form action="//moneyguruinvest.us13.list-manage.com/subscribe/post?u=<?php echo $campaign; ?>&amp;id=<?php echo $list; ?>" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
					<div id="mc_embed_signup_scroll" class="layout">
						<div class="layout__item">
							<input type="email" value="" name="EMAIL" class="email input input--simple" id="mce-EMAIL" placeholder="Your Email" required>
						</div><!-- layout__item large-and-up-3/4 -->

						<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
						<div style="position: absolute; left: -5000px;">
							<input type="text" name="b_<?php echo $campaign; ?>_<?php echo $list; ?>" tabindex="-1" value="">
						</div>

						<div class="layout__item">
							<button onchange="return this.form.submit()" name="subscribe" id="mc-embedded-subscribe" class="btn btn--primary btn--subscribe mt-"><?php echo $subscribe; ?></button>
						</div><!-- layout__item large-and-up-1/4 -->
					</div><!-- layout -->
				</form>
			</div><!-- #mc_embed_signup -->
			<!--End mc_embed_signup-->
		</div><!-- container--guru subscribe__content -->

		<div class="subscribe__thank-you">
			<?php echo get_template_part( 'partials/content/subscribe', 'thank-you' ); ?>
		</div><!-- subscribe__thank-you -->
	</section><!-- subscribe subscribe--res -->
</div><!-- #subscribe -->

<div class="subsribe-lightbox magnific-popup mfp-hide magnific-animate" id="subscribe-lightbox">
	<div class="subscribe-popup__content">
		<div class="mfp-close"><?php echo house_svg_icon( 'close' ); ?></div>

		<div class="subscribe-popup__content--top text-center ph- pv+ medium-ph large-and-up-ph+ large-and-up-pv++">
			<?php if ( get_field( 'subscribe_popup_title' ) ) : ?>
				<h2><?php the_field( 'subscribe_popup_title' ); ?></h2>
			<?php endif; // get_field( 'subscribe_popup_title' ) ?>

			<?php if ( get_field( 'subscribe_section_text' ) ) : ?>
				<p class="medium-and-up-ph+ mb0"><?php the_field( 'subscribe_section_text' ); ?></p>
			<?php endif; // get_field( 'subscribe_section_text' ) ?>
		</div><!-- subscribe-popup__content--top text-center ph- pv+ medium-ph large-and-up-ph+ large-and-up-pv++ -->

		<!-- Begin MailChimp Signup Form -->
		<div id="mc_embed_signup" class="subscribe-popup__content--bottom text-center pv+ ph large-and-up-ph+ large-and-up-pv++">
			<form action="//moneyguruinvest.us13.list-manage.com/subscribe/post?u=<?php echo $campaign; ?>&amp;id=<?php echo $list; ?>" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
				<div id="mc_embed_signup_scroll">
					<input type="email" value="" name="EMAIL" class="email input input--primary mr" id="mce-EMAIL" placeholder="Your Email" required>

					<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
					<div style="position: absolute; left: -5000px;">
						<input type="text" name="b_<?php echo $campaign; ?>_<?php echo $list; ?>" tabindex="-1" value="">
					</div>

					<button onchange="return this.form.submit()" name="subscribe" id="mc-embedded-subscribe" class="btn btn--yellow btn--subscribe-popup"><?php echo $subscribe; ?></button>
				</div><!-- #mc_embed_signup_scroll -->
			</form>
		</div><!-- #mc_embed_signup -->
		<!--End mc_embed_signup-->
	</div><!--s ubscribe-popup__content -->

	<div class="subscribe-popup__content--thank-you pv++">
		<div class="mfp-close"><?php echo house_svg_icon( 'close' ); ?></div>
		<?php echo get_template_part( 'partials/content/subscribe', 'thank-you' ); ?>
	</div><!-- subscribe-popup__content--thank-you pv++ -->
</div><!-- subsribe-lightbox magnific-popup mfp-hide magnific-animate -->