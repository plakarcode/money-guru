<?php
/**
 * Main navigation template part
 *
 * Template part for rendering main navigation.
 *
 * @link https://codex.wordpress.org/Function_Reference/wp_nav_menu
 *
 * @package WordPress
 */
add_filter( 'wp_nav_menu_items', 'add_last_nav_item', 10, 2 );
?>
<nav id="site-navigation" class="header__nav text-right" role="navigation">
	<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav', 'container' => false ) ); ?>
</nav><!-- #site-navigation -->
<?php

remove_filter( 'wp_nav_menu_items', 'add_last_nav_item', 10, 2 );