<?php
/**
 * Featured images
 *
 * Get default and theme defined image sizes for featured images.
 *
 * @package WordPress
 */
/**
 * Get post thumbnail image
 *
 * Get featured image size 'thumbnail' (150x150) without link to post.
 *
 * @see inc/images/get-the-image.php
 *
 * @return string The HTML for the image.
 */
function get_post_image_thumbnail() {
	$args = array(
		/* Post the image is associated with. */
		'post_id'            => get_the_ID(),

		/* Methods of getting an image (in order). */
		'meta_key'           => array( 'thumbnail' ), // array|string
		'the_post_thumbnail' => true,
		'attachment'         => false, // if no thumbnail then it gets first attached image
		'image_scan'         => false,
		'callback'           => null,
		'default_image'      => false,

		/* Attachment-specific arguments. */
		'size'               => isset( $_wp_additional_image_sizes['post-thumbnail'] ) ? 'post-thumbnail' : 'thumbnail',
		'order_of_image'     => 1,

		/* Format/display of image. */
		'link_to_post'       => false, // just image for now
		'image_class'        => 'post-image post-image-thumbnail',
		'width'              => false,
		'height'             => false,
		'before'             => '',
		'after'              => '',

		/* Captions. */
		'caption'            => false, // Default WP [caption] requires a width.
	);
	return get_the_image( $args );
}

/**
 * Get post thumbnail image link
 *
 * Get featured image size 'thumbnail' (150x150) with link to post.
 *
 * @see inc/images/get-the-image.php
 *
 * @return string The HTML for the image.
 */
function get_post_image_thumbnail_link() {
	$args = array(
		/* Post the image is associated with. */
		'post_id'            => get_the_ID(),

		/* Methods of getting an image (in order). */
		'meta_key'           => array( 'thumbnail' ), // array|string
		'the_post_thumbnail' => true,
		'attachment'         => false, // if no thumbnail then it gets first attached image
		'image_scan'         => false,
		'callback'           => null,
		'default_image'      => false,

		/* Attachment-specific arguments. */
		'size'               => isset( $_wp_additional_image_sizes['post-thumbnail'] ) ? 'post-thumbnail' : 'thumbnail',
		'order_of_image'     => 1,

		/* Format/display of image. */
		'link_to_post'       => true,
		'image_class'        => 'post-image post-image-thumbnail post-image-link',
		'width'              => false,
		'height'             => false,
		'before'             => '',
		'after'              => '',

		/* Captions. */
		'caption'            => false, // Default WP [caption] requires a width.
	);
	return get_the_image( $args );
}

/**
 * Get post medium image
 *
 * Get featured image size 'medium' (max300 x max300) without link to post.
 *
 * @see inc/images/get-the-image.php
 *
 * @return string The HTML for the image.
 */
function get_post_image_medium() {
	$args = array(
		/* Post the image is associated with. */
		'post_id'            => get_the_ID(),

		/* Methods of getting an image (in order). */
		'meta_key'           => array( 'medium' ), // array|string
		'the_post_thumbnail' => true,
		'attachment'         => false, // if no thumbnail then it gets first attached image
		'image_scan'         => false,
		'callback'           => null,
		'default_image'      => false,

		/* Attachment-specific arguments. */
		'size'               => isset( $_wp_additional_image_sizes['medium'] ) ? 'medium' : 'medium',
		'order_of_image'     => 1,

		/* Format/display of image. */
		'link_to_post'       => false, // just image for now
		'image_class'        => 'post-image post-image-medium',
		'width'              => false,
		'height'             => false,
		'before'             => '',
		'after'              => '',

		/* Captions. */
		'caption'            => false, // Default WP [caption] requires a width.
	);
	return get_the_image( $args );
}

/**
 * Get post medium image link
 *
 * Get featured image size 'medium' (max300 x max300) with link to post.
 *
 * @see inc/images/get-the-image.php
 *
 * @return string The HTML for the image.
 */
function get_post_image_medium_link() {
	$args = array(
		/* Post the image is associated with. */
		'post_id'            => get_the_ID(),

		/* Methods of getting an image (in order). */
		'meta_key'           => array( 'medium' ), // array|string
		'the_post_thumbnail' => true,
		'attachment'         => false, // if no thumbnail then it gets first attached image
		'image_scan'         => false,
		'callback'           => null,
		'default_image'      => false,

		/* Attachment-specific arguments. */
		'size'               => isset( $_wp_additional_image_sizes['medium'] ) ? 'medium' : 'medium',
		'order_of_image'     => 1,

		/* Format/display of image. */
		'link_to_post'       => true,
		'image_class'        => 'post-image post-image-medium post-image-link',
		'width'              => false,
		'height'             => false,
		'before'             => '',
		'after'              => '',

		/* Captions. */
		'caption'            => false, // Default WP [caption] requires a width.
	);
	return get_the_image( $args );
}

/**
 * Get post large image
 *
 * Get featured image size 'large' (max1024 x max1024) without link to post.
 *
 * @see inc/images/get-the-image.php
 *
 * @return string The HTML for the image.
 */
function get_post_image_large() {
	$args = array(
		/* Post the image is associated with. */
		'post_id'            => get_the_ID(),

		/* Methods of getting an image (in order). */
		'meta_key'           => array( 'large' ), // array|string
		'the_post_thumbnail' => true,
		'attachment'         => false, // if no thumbnail then it gets first attached image
		'image_scan'         => false,
		'callback'           => null,
		'default_image'      => false,

		/* Attachment-specific arguments. */
		'size'               => isset( $_wp_additional_image_sizes['large'] ) ? 'large' : 'large',
		'order_of_image'     => 1,

		/* Format/display of image. */
		'link_to_post'       => false, // just image for now
		'image_class'        => 'post-image post-image-large',
		'width'              => false,
		'height'             => false,
		'before'             => '',
		'after'              => '',

		/* Captions. */
		'caption'            => false, // Default WP [caption] requires a width.
	);
	return get_the_image( $args );
}

/**
 * Get post large image link
 *
 * Get featured image size 'large' (max1024 x max1024) with link to post.
 *
 * @see inc/images/get-the-image.php
 *
 * @return string The HTML for the image.
 */
function get_post_image_large_link() {
	$args = array(
		/* Post the image is associated with. */
		'post_id'            => get_the_ID(),

		/* Methods of getting an image (in order). */
		'meta_key'           => array( 'large' ), // array|string
		'the_post_thumbnail' => true,
		'attachment'         => false, // if no thumbnail then it gets first attached image
		'image_scan'         => false,
		'callback'           => null,
		'default_image'      => false,

		/* Attachment-specific arguments. */
		'size'               => isset( $_wp_additional_image_sizes['large'] ) ? 'large' : 'large',
		'order_of_image'     => 1,

		/* Format/display of image. */
		'link_to_post'       => true,
		'image_class'        => 'post-image post-image-large post-image-link',
		'width'              => false,
		'height'             => false,
		'before'             => '',
		'after'              => '',

		/* Captions. */
		'caption'            => false, // Default WP [caption] requires a width.
	);
	return get_the_image( $args );
}

/**
 * Get post featured-preview image
 *
 * Get featured image size 'featured-preview' (100x75) without link to post.
 *
 * @see inc/images/get-the-image.php
 *
 * @return string The HTML for the image.
 */
function get_post_image_featured_preview() {
	$args = array(
		/* Post the image is associated with. */
		'post_id'            => get_the_ID(),

		/* Methods of getting an image (in order). */
		'meta_key'           => array( 'featured-preview' ), // array|string
		'the_post_thumbnail' => true,
		'attachment'         => false, // if no thumbnail then it gets first attached image
		'image_scan'         => false,
		'callback'           => null,
		'default_image'      => false,

		/* Attachment-specific arguments. */
		'size'               => isset( $_wp_additional_image_sizes['featured-preview'] ) ? 'featured-preview' : 'featured-preview',
		'order_of_image'     => 1,

		/* Format/display of image. */
		'link_to_post'       => false, // just image for now
		'image_class'        => 'post-image post-image-featured-preview',
		'width'              => false,
		'height'             => false,
		'before'             => '',
		'after'              => '',

		/* Captions. */
		'caption'            => false, // Default WP [caption] requires a width.
	);
	return get_the_image( $args );
}

/**
 * Get post featured-preview image link
 *
 * Get featured image size 'featured-preview' (100x75) with link to post.
 *
 * @see inc/images/get-the-image.php
 *
 * @return string The HTML for the image.
 */
function get_post_image_featured_preview_link() {
	$args = array(
		/* Post the image is associated with. */
		'post_id'            => get_the_ID(),

		/* Methods of getting an image (in order). */
		'meta_key'           => array( 'featured-preview' ), // array|string
		'the_post_thumbnail' => true,
		'attachment'         => false, // if no thumbnail then it gets first attached image
		'image_scan'         => false,
		'callback'           => null,
		'default_image'      => false,

		/* Attachment-specific arguments. */
		'size'               => isset( $_wp_additional_image_sizes['featured-preview'] ) ? 'featured-preview' : 'featured-preview',
		'order_of_image'     => 1,

		/* Format/display of image. */
		'link_to_post'       => true,
		'image_class'        => 'post-image post-image-featured-preview post-image-link',
		'width'              => false,
		'height'             => false,
		'before'             => '',
		'after'              => '',

		/* Captions. */
		'caption'            => false, // Default WP [caption] requires a width.
	);
	return get_the_image( $args );
}

/**
 * Get post full-featured image
 *
 * Get featured image size 'full-featured' (2000x0) without link to post.
 *
 * @see inc/images/get-the-image.php
 *
 * @return string The HTML for the image.
 */
function get_post_image_full_featured() {
	$args = array(
		/* Post the image is associated with. */
		'post_id'            => get_the_ID(),

		/* Methods of getting an image (in order). */
		'meta_key'           => array( 'full-featured' ), // array|string
		'the_post_thumbnail' => true,
		'attachment'         => false, // if no thumbnail then it gets first attached image
		'image_scan'         => false,
		'callback'           => null,
		'default_image'      => false,

		/* Attachment-specific arguments. */
		'size'               => isset( $_wp_additional_image_sizes['full-featured'] ) ? 'full-featured' : 'full-featured',
		'order_of_image'     => 1,

		/* Format/display of image. */
		'link_to_post'       => false, // just image for now
		'image_class'        => 'post-image post-image-full-featured',
		'width'              => false,
		'height'             => false,
		'before'             => '',
		'after'              => '',

		/* Captions. */
		'caption'            => false, // Default WP [caption] requires a width.
	);
	return get_the_image( $args );
}

/**
 * Get post full-featured image link
 *
 * Get featured image size 'full-featured' (2000x0) with link to post.
 *
 * @see inc/images/get-the-image.php
 *
 * @return string The HTML for the image.
 */
function get_post_image_full_featured_link() {
	$args = array(
		/* Post the image is associated with. */
		'post_id'            => get_the_ID(),

		/* Methods of getting an image (in order). */
		'meta_key'           => array( 'full-featured' ), // array|string
		'the_post_thumbnail' => true,
		'attachment'         => false, // if no thumbnail then it gets first attached image
		'image_scan'         => false,
		'callback'           => null,
		'default_image'      => false,

		/* Attachment-specific arguments. */
		'size'               => isset( $_wp_additional_image_sizes['full-featured'] ) ? 'full-featured' : 'full-featured',
		'order_of_image'     => 1,

		/* Format/display of image. */
		'link_to_post'       => true,
		'image_class'        => 'post-image post-image-full-featured post-image-link',
		'width'              => false,
		'height'             => false,
		'before'             => '',
		'after'              => '',

		/* Captions. */
		'caption'            => false, // Default WP [caption] requires a width.
	);
	return get_the_image( $args );
}