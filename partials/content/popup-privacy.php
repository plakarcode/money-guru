<?php
/**
 * Privacy Popup
 *
 * Template part for rendering privacy policy popup
 *
 * @package WordPress
 */
if ( ! get_field( 'show_privacy_policy' ) ) {
	return;
}
?>

<div class="privacy-lightbox magnific-popup mfp-hide magnific-animate" id="privacy-lightbox">
	<div class="privacy-popup__content ph+ pv+ large-and-up-ph+">
		<div class="mfp-close"><?php echo house_svg_icon( 'close' ); ?></div>

		<?php if ( get_field( 'privacy_popup_title' ) ) : ?>
			<h2><?php the_field( 'privacy_popup_title' ); ?></h2>
		<?php endif; // get_field( 'privacy_popup_title' )?>

		<?php
			/**
			 * Start flexible fields
			 */
			while ( the_flexible_field( 'privacy_popup_content' ) ) :

				// START HEADING
				if ( get_row_layout() == 'popup_heading' ) : ?>
					<h3><?php the_sub_field( 'heading' ); ?></h3>
				<?php // END HEADING

				// START HIGHLIGHTED
				elseif ( get_row_layout() == 'popup_highlighted' ) : ?>
					<p class="featured"><?php the_sub_field( 'highlighted_paragraph' ); ?></p>
				<?php // END HIGHLIGHTED

				// START paragraph
				elseif ( get_row_layout() == 'popup_paragraph' ) : ?>
					<p><?php the_sub_field( 'paragraph' ); ?></p>
				<?php // END paragraph

				endif; // get_row_layout()

			endwhile; // the_flexible_field( 'privacy_popup_content' )
		?>
	</div><!-- privacy-popup__content ph+ pv+ large-and-up-ph+ -->
</div><!-- privacy-lightbox magnific-popup mfp-hide magnific-animate -->