<?php
/**
 * Share Lightbox
 *
 * Template part for rendering share lightbox
 *
 * @package WordPress
 */
global $globalSite;
?>
<div id='share-lightbox' class="share-lightbox magnific-popup mfp-hide magnific-animate">
	<div class="share-popup__content">
		<div class="mfp-close">
			<?php echo house_svg_icon('close'); ?>
		</div>
		<div class="layout">
			<div class="layout__item 1/2 text-center">
				<?php echo get_share_link( 'facebook', $globalSite['home'], '', '', 'icon-facebook-lg' ); ?>
			</div>
			<div class="layout__item 1/2 text-center">
				<?php echo get_share_link( 'twitter', $globalSite['home'], 'Check out MoneyGuru - an online investment platform combined, financial news and portfolio monitoring.', 'MoneyGuru', 'icon-twitter-lg' ); ?>
			</div>
		</div>
	</div>
</div>