<?php
/**
 * Branding graphics
 *
 * Functions to retrieve graphics uploaded via Branding Options page
 * (logo, favicon)
 *
 * @package WordPress
 */
/**
 * Hooks
 */
add_action( 'wp_head', 'get_site_favicon' );

/**
 * Get site logo
 *
 * Get the image uploaded via Branding Options page and place it in header.
 *
 * @return mix Returns site logo with link to home markup
 */
function get_site_logo() {
	global $globalSite;

	if ( house_is_plugin_active( 'advanced-custom-fields-pro/acf.php' ) ) {
		$logo_array = get_field( 'site_logo', 'options' );

		if ( $logo_array ) {

			$logo_src = $logo_array['url'];

			$logo = '<img src="' . esc_url( $logo_src ) . '" alt="' . $globalSite['name'] . ' - ' . $globalSite['description'] . '" />';

			$link = '<a class="site-logo" href="' . esc_url( $globalSite['home'] ) . '" title="' . $globalSite['name'] . '" rel="home">' . $logo . '</a>';

			return $link;
		}
	}
}
/**
 * Get site title
 *
 * @return string Returns home link site title
 */
function get_site_title() {
	global $globalSite;

	$link = '<h1><a class="site-title" href="' . esc_url( $globalSite['home'] ) . '" title="' . $globalSite['name'] . '" rel="home">' . $globalSite['name'] . '</a></h1>';

	return $link;
}
/**
 * Get site description
 *
 * @return string Returns site description
 */
function get_site_description() {
	global $globalSite;

	$description = '<div class="site-description">' . $globalSite['description'] . '</div>';

	return $description;
}
/**
 * Get site favicon
 *
 * Get the image uploaded via Branding Options page and
 * place it in document's head. This function is attached
 * to 'wp_head' action hook.
 *
 * @return mix Returns site favicon markup
 */
function get_site_favicon() {
	global $globalSite;

	$favicon = $globalSite['template_url'] . '/favicon.ico';

	if ( $favicon ) { ?>

		<link rel="shortcut icon" href="<?php echo esc_url( $favicon ); ?>">

	<?php
	}
}