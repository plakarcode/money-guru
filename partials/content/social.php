<?php
/**
 * Social
 *
 * Template part for rendering social profile links
 *
 * @package WordPress
 */
$profiles = get_field( 'social_profiles' );
/**
 * Prepare custom array
 * @var array
 */
$soc = [];
if ( $profiles ) :
	foreach ( $profiles as $profile ) {
		$soc[$profile['network']] = $profile['url'];
	}
endif; // $profiles

if ( $soc ) :
	foreach ( $soc as $key => $value ) : ?>
		<li>
			<a href="<?php echo $value; ?>" target="_blank">
				<div class="social-button"><?php echo house_svg_icon( $key ); ?></div>
			</a>
		</li>
	<?php endforeach; // $soc as $key => $value
endif; // $soc