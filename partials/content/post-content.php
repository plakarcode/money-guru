<?php
/**
 * Content
 *
 * Template part for rendering content for posts and pages
 *
 * @package WordPress
 */

if ( is_search() ) { ?>
	<div class="excerpt">
		<?php the_excerpt(); ?>
	</div><?php
}
else { ?>
	<div class="content">
		<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'house' ) ); ?>
	</div>

	<?php wp_link_pages( array(
		'before' => '<div class="page-links"><span class="pages-text">' . __( 'Pages:', 'house' ),
		'after' => '</span></div>',
		'separator' => '</span><span class="pages-item">'
		)
	);
}