<?php
/**
 * Here include all external api related functions
 */
/**
 * Video API and functions
 */
// youtube functions
include( get_template_directory() . '/inc/api/youtube-functions.php' );
// vimeo functions
include( get_template_directory() . '/inc/api/vimeo-functions.php' );
// video functions
include( get_template_directory() . '/inc/api/video-functions.php' );