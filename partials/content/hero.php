<?php
/**
 * Hero
 *
 * Template part for rendering hero section
 *
 * @package WordPress
 */
/**
 * Get the image src
 * @var array
 */
$image = get_field( 'hero_image' );
if ( $image ) {
	$src = $image['url'];
} else {
	$src = '';
}
/**
 * Get title
 * @var string
 */
$title = get_field( 'hero_title' );
/**
 * Get intro p
 * @var string
 */
$intro = get_field( 'hero_intro' );
/**
 * Get button label
 * @var string
 */
$subscribe_label = get_field( 'hero_subscribe_button_label' );
if ( $subscribe_label ) {
	$subscribe = $subscribe_label;
} else {
	$subscribe = 'Subscribe Now';
}
/**
 * Get video label
 * @var string
 */
$video_label = get_field( 'hero_video_label' );
if ( $video_label ) {
	$video_more = $video_label;
} else {
	$video_more = 'Find out more';
}
/**
 * Get video url
 */
$video = get_field( 'hero_video_url' );
/**
 * Get video image
 * @var array
 */
$video_image = get_field( 'hero_video_image' );
if ( $video_image ) {
	$thumbnail = $video_image['url'];
} else {
	$thumbnail = set_video_image_src( $video );
}
?>

<section class="hero" style="background-image:url(<?php echo $src; ?>)">
	<div class="container--guru">
		<div class="tableize tableize--middle tableize--full">
			<div class="tableize__cell hero__item1">
				<div class="hero__content">
					<div class="tableize tableize--middle">
						<div class="tableize__cell">
							<?php if ( $title ) : ?>
								<h1 class="mb-"><?php echo $title; ?></h1>
							<?php endif; // $title

							if ( $intro ) : ?>
								<p class="pr+"><?php echo $intro; ?></p>
							<?php endif; // $intro ?>

							<a href="#subscribe-lightbox" class="btn btn--primary btn--openpopup"><?php echo $subscribe; ?></a>
						</div><!-- tableize__cell -->

						<?php if ( $video ) : ?>
							<div class="tableize__cell hero__item2">
								<div class="ml">
									<div class="js-playvideo hero__video text-center" style="background-image:url(<?php echo $thumbnail; ?>);">
										<div class="hero__video--play">
											<?php echo house_svg_icon( 'play' ); ?>
											<p class="type-uppercase"><?php echo $video_more; ?></p>
										</div><!-- hero__video--play -->
									</div><!-- js-playvideo hero__video text-center -->
									<div class="video-wrapper">
										<?php echo build_video_iframe( $video, 'video-desktop', '640', '360' ); ?>
									</div><!-- video-wrapper -->
								</div><!-- ml -->
							</div><!-- tableize__cell hero__item2 -->
						<?php endif; // $video ?>
					</div><!-- tableize tableize--middle -->
				</div><!-- hero__content -->
			</div><!-- tableize__cell hero__item1 -->
		</div><!-- tableize tableize--middle tableize--full -->
	</div><!-- container--guru -->
</section><!-- hero desktop/laptop -->

<section class="hero hero--tablet" style="background-image:url(<?php echo $src; ?>)">
	<div class="container--guru">
		<div class="tableize tableize--middle tableize--full">
			<div class="tableize__cell">
				<div class="hero__title">
					<?php if ( $title ) : ?>
						<h1 class="mb-"><?php echo $title; ?></h1>
					<?php endif; // $title

					if ( $intro ) : ?>
						<p><?php echo $intro; ?></p>
					<?php endif; // $intro ?>

					<a href="#subscribe-lightbox" class="btn btn--primary btn--openpopup"><?php echo $subscribe; ?></a>
				</div><!-- hero__title -->

				<?php if ( $video ) : ?>
					<div class="js-playvideo hero__video text-center" style="background-image:url(<?php echo $thumbnail; ?>);">
						<div class="hero__video--play">
							<?php echo house_svg_icon( 'play' ); ?>
							<p class="type-uppercase"><?php echo $video_more; ?></p>
						</div><!-- hero__video--play -->
					</div><!-- js-playvideo hero__video text-center -->

					<div class="video-wrapper">
						<?php echo build_video_iframe( $video, 'video-tablet', '640', '360' ); ?>
					</div><!-- video-wrapper -->
				<?php endif; // $video ?>

			</div><!-- tableize__cell -->
		</div><!-- tableize tableize--middle tableize--full -->
	</div><!-- container--guru -->
</section><!-- hero tablet -->

<section class="hero hero--mobile" style="background-image:url(<?php echo $src; ?>)">
	<div class="container--guru">
		<div class="hero__title">
			<?php if ( $title ) : ?>
				<h1 class="mb"><?php echo $title; ?></h1>
			<?php endif; // $title

			if ( $intro ) : ?>
				<p><?php echo $intro; ?></p>
			<?php endif; // $intro ?>

			<a href="#subscribe-lightbox" class="btn btn--primary btn--openpopup"><?php echo $subscribe; ?></a>
		</div><!-- hero__title -->
	</div><!-- container--guru -->
</section><!-- hero hero--mobile -->

<?php if ( $video ) : ?>
	<div class="video--mobile">
		<div class="js-playvideo hero__video text-center" style="background-image:url(<?php echo $thumbnail; ?>);">
			<?php echo house_svg_icon( 'play' ); ?>
		</div><!-- js-playvideo hero__video text-center -->

		<div class="video-wrapper">
			<?php echo build_video_iframe( $video, 'video-mob', '640', '360' ); ?>
		</div><!-- video-wrapper -->
	</div><!-- video mobile -->
<?php endif; // $video ?>