<?php
/**
 * Here include all navigation related functions
 *
 * @todo  single post navigation
 */

// textual and numbered archive pagination
include( get_template_directory() . '/inc/navigations/pagination.php' );
// custom navigations functionality
include( get_template_directory() . '/inc/navigations/navigation.php' );