<?php
/**
 * Singular titles
 *
 * Template part for rendering titles for posts and pages
 *
 * @package WordPress
 */

// singular - post or page but not home page
if ( is_singular() && ! is_front_page() ) { ?>
	<h1 class="entry-title"><?php the_title(); ?></h1>
<?php }

// on archive pages (for posts)
elseif ( ! is_singular() ) { ?>
	<h2 class="entry-title">
		<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'house' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
	</h2>
<?php }