<?php
/**
 * The template for displaying all pages by default.
 *
 * This template is without sidebar. For adding sidebars on each side, use page templates.
 *
 */
get_header(); ?>

	<?php
		/**
		 * Get hero section
		 */
		get_template_part( 'partials/content/hero' );
		/**
		 * Get benefits section
		 */
		get_template_part( 'partials/content/benefits' );
		/**
		 * Get subscribe section
		 */
		get_template_part( 'partials/content/subscribe' );
		/**
		 * Get social section
		 */
		get_template_part( 'partials/content/share', 'lightbox' ); ?>

	<?php
		if ( ! is_front_page() ) :
			while ( have_posts() ) : the_post();

				get_template_part( 'content', 'page' );

			endwhile; // have_posts()
		endif; // ! is_front_page()
	?>

<?php get_footer(); ?>