<?php
/**
 * Here include all content related custom functions
 */

// excerpts
include( get_template_directory() . '/inc/content/excerpts.php' );
// excerpts
include( get_template_directory() . '/inc/content/content.php' );
// footer copyrights
include( get_template_directory() . '/inc/content/footer-copyrights.php' );
// body and article classes
include( get_template_directory() . '/inc/content/content-classes.php' );