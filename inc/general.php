<?php
/**
 * Custom functions not related to anything specific
 */

/**
 * Trim string
 *
 * When we need limited number of characters for a string (i.e excerpt )
 *
 * @link http://stackoverflow.com/questions/12423407/limiting-characters-retrived-from-a-database-field#answer-12423453
 *
 * @param  string 	$str    		String to be trimmed
 * @param  integer 	$length 		Number of characters
 * @return string         			Trimmed string
 */
function trim_string( $str, $length, $after = '...' ) {

	if ( ! ( strlen( $str ) <= $length ) ) {
		$str = substr( $str, 0, strpos( $str, ' ', $length ) ) . ' ' . $after;
	}

	return $str;
}

/**
 * Admin menu Chrome fix
 *
 * Dashboard menu gets broken on hover in Chrome.
 *
 * @link https://core.trac.wordpress.org/ticket/33199
 * @link http://wordpress.stackexchange.com/questions/200096/admin-sidebar-items-overlapping-in-admin-panel
 */
add_action( 'admin_enqueue_scripts', 'chrome_fix' );

function chrome_fix() {

	if ( strpos( $_SERVER[ 'HTTP_USER_AGENT' ], 'Chrome' ) !== false ) {
		wp_add_inline_style( 'wp-admin', '#adminmenu { transform: translateZ(0) }' );
	}
}